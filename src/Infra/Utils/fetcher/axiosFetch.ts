// import { AxiosResponse } from 'axios';
// import { IFetch } from './interface'
// import { AxiosRequestConfig } from 'axios';
// class AxiosFetch implements IFetch {
//   private api
//   constructor() {
//     this.api = axios.create({})
//   }
//   // Método GET
//   async get(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
//     return await this.api.get(url, config)
//       .then(response => {
//         console.log(response.data);
//         return response.data
//       })
//       .catch(error => {
//         console.error('GET Error:', error);
//         return error
//       });
//   }
//   post(url: string, data: any, config?: Record<string, unknown>) {
//     return Promise.resolve()
//   }
//   put(url: string, data: any, config?: Record<string, unknown>) {
//     return Promise.resolve()
//   }
//   delete(url: string, config?: Record<string, unknown>) {
//     return Promise.resolve()
//   }
// }

import https from 'https';
import { URL } from 'url';
import { IncomingMessage } from 'http';

interface HttpRequestConfig {
  method?: string;
  headers?: Record<string, string>;
  timeout?: number;
  params?: Record<string, string | number>;
}

export interface HttpResponse<T> {
  status: number;
  statusText: string;
  data: T;
}

class NativeFetch {
  private buildUrl(url: string, params?: Record<string, string | number>): string {
    const urlObj = new URL(url);

    if (params) {
      Object.keys(params).forEach((key) => urlObj.searchParams.append(key, params[key].toString()));
    }

    return urlObj.toString();
  }

  get<T>(url: string, config?: HttpRequestConfig): Promise<HttpResponse<T>> {
    const fullUrl = this.buildUrl(url, config?.params);

    return new Promise((resolve, reject) => {
      const req = https.get(fullUrl, { headers: config?.headers, timeout: config?.timeout }, (res: IncomingMessage) => {
        let data = '';

        res.on('data', (chunk) => {
          data += chunk;
        });

        res.on('end', () => {
          const response: HttpResponse<T> = {
            status: res.statusCode || 500,
            statusText: res.statusMessage || '',
            data: JSON.parse(data) as T,
          };
          resolve(response);
        });
      });

      req.on('error', (err) => {
        reject(err);
      });

      req.end();
    });
  }

  post<T>(url: string, data: any, config?: HttpRequestConfig): Promise<HttpResponse<T>> {
    const fullUrl = this.buildUrl(url, config?.params);

    return new Promise((resolve, reject) => {
      const req = https.request(
        fullUrl,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            ...(config?.headers || {}),
          },
          timeout: config?.timeout,
        },
        (res: IncomingMessage) => {
          let responseData = '';

          res.on('data', (chunk) => {
            responseData += chunk;
          });

          res.on('end', () => {
            const response: HttpResponse<T> = {
              status: res.statusCode || 500,
              statusText: res.statusMessage || '',
              data: JSON.parse(responseData) as T,
            };
            resolve(response);
          });
        }
      );

      req.on('error', (err) => {
        reject(err);
      });

      req.write(JSON.stringify(data));
      req.end();
    });
  }
}

export default NativeFetch;
