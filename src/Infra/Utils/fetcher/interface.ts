export interface IFetch {
  get: (url: string, config?: Record<string, unknown>) => Promise<any>
  post: (url: string, data: any, config?: Record<string, unknown>) => Promise<any>
  put: (url: string, data: any, config?: Record<string, unknown>) => Promise<any>
  delete: (url: string, config?: Record<string, unknown>) => Promise<any>
}
