import {
  EmailServiceRequest,
  EmailService as IEmailService
} from '../../Modules/account/interfaces'
import nodemailer from 'nodemailer'
import * as dotenv from 'dotenv'
dotenv.config()

export class EmailService implements IEmailService {
  public async sendEmail(body: EmailServiceRequest) {
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: process.env.EMAIL_SENDER,
        pass: process.env.EMAIL_SENDER_PASSWORD
      },
      tls: {
        rejectUnauthorized: false // Permitir certificados autofirmados
      }
    })

    const mailOptions = {
      from: process.env.EMAIL_SENDER,
      to: body.to,
      subject: body.subject,
      html: `<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Correo Electrónico</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                background-color: #bcbdac;
                color: #ffffff;
            }
             a.button {
                color: #fce5cd;
                text-decoration: none;
            }
            .container {
                max-width: 600px;
                margin: 0 auto;
                padding: 20px;
                background-color: #bcbdac;
                border-radius: 8px;
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
                color: #333;
            }
            .button {
                display: inline-block;
                padding: 15px 25px;
                font-size: 16px;
                font-weight: bold;
                text-decoration: none;
                color: #fce5cd;
                background-color: #5b3417;
                border-radius: 5px;
                border: none;
                text-align: center;
            }
            .button:hover {
                background-color: #6d1f9a;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>¡Bienvenido!</h1>
            <p>Gracias por registrarte. Haz clic en el botón a continuación para completar tu registro:</p>
            <a href="${body.text}" class="button">Haz clic aquí</a>
            <p>Si tienes alguna pregunta, no dudes en contactarnos.</p>
        </div>
    </body>
    </html>`
    }

    try {
      await transporter.sendMail(mailOptions)
      return true
    } catch (error) {
      console.error('Error al enviar el correo:', error)
      return false
    }
  }
}
