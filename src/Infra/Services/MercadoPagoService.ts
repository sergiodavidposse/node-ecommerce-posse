import { ProcessOrderResponse } from '../../Modules/order/domain/ProcessOrderResponse'
import { MercadoPagoPreference } from '../../Modules/order/domain/MercadoPagoPreference'
import { MercadoPagoConfig, Preference, Payment } from 'mercadopago'
import { PreferenceResponse } from './interface'
import { ProcessOrderService } from '../../Modules/order/domain/ProcessOrderService'
import { PaymentResponse } from 'mercadopago/dist/clients/payment/commonTypes'
import { PaymentInfoMapper } from './PaymentInfoMapper'
import * as dotenv from 'dotenv'
import NativeFetch from '../../Infra/Utils/fetcher/axiosFetch'
dotenv.config()

const client = new MercadoPagoConfig({
  accessToken: process.env.MP_ACCESSTOKEN || ''
})

export class MercadoPagoService implements ProcessOrderService {
  async processOrder(data: MercadoPagoPreference[]): Promise<ProcessOrderResponse | null> {
    const items = data.map((item: MercadoPagoPreference) => {
      return {
        id: item.id,
        title: item.name,
        unit_price: item.price,
        quantity: item.quantity,
        picture_url: item.thumbnail,
        currency_id: item.currency,
        category_id: 'art'
      }
    })
    const preference: PreferenceResponse = await new Preference(client).create({
      body: {
        auto_return: 'approved',
        redirect_urls: {
          success: 'https://vite-react-ecommerce-posse.onrender.com/success',
          pending: 'https://vite-react-ecommerce-posse.onrender.com/pending',
          failure: 'https://vite-react-ecommerce-posse.onrender.com/error'
        },
        back_urls: {
          success: 'https://vite-react-ecommerce-posse.onrender.com/success',
          pending: 'https://vite-react-ecommerce-posse.onrender.com/pending',
          failure: 'https://vite-react-ecommerce-posse.onrender.com/error'
        },
        items
      }
    })
    if (preference?.init_point && preference.sandbox_init_point && preference.id) {
      return {
        preferenceId: preference.id,
        paymentUrl: preference.init_point,
        paymentSandboxUrl: preference.sandbox_init_point
      }
    }
    return null
  }

  // solo funciona con el token de la cuenta real y no de las cuentas de prueba
  public async getPaymentInfo(paymentId: string) {
    const payment = new Payment(client)
    const result: PaymentResponse = await payment.get({ id: paymentId })
    if (!result) return null
    const mappedPaymentInfo = PaymentInfoMapper.map(result)
    return mappedPaymentInfo
  }
  // solo funciona con el token de la cuenta real y no de las cuentas de prueba
  // pero no necesito instanciar de nuevo mi client ya que es un fetch directo
  public async getPaymentInfoAPI(paymentId: string) {
    const fetcher = new NativeFetch()
    const url = `${process.env.MP_URL_PAYMENT_API}/${paymentId}` || ''
    const token = process.env.MP_ACCESSTOKEN_MAIN || ''
    if(!token) throw new Error('Token not provided')
    const result = await fetcher.get(url, {headers: {Authorization: `Bearer ${token}`}})
    if (!result) return null
    const mappedPaymentInfo = PaymentInfoMapper.map(result.data)
    return mappedPaymentInfo
  }
}
export default MercadoPagoService
