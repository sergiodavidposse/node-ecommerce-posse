import Connection from '../Database/connection'
import { Account, Session, SessionRepository } from '../../Modules/account/interfaces'
import { AuthService as IAuthService, RoleType } from '../../Modules/account/interfaces/AuthService'
import jwt, { JwtPayload } from 'jsonwebtoken'
import * as dotenv from 'dotenv'
dotenv.config()

// const mockRefreshTokens: string[] = []

export class AuthService implements IAuthService {
  constructor(private sessionRepository: SessionRepository) {}
  generateToken(userAccount: Account) {
    const { refreshToken, accessToken } = this.signTokenAndRefreshToken(userAccount)
    console.log(new Date(), refreshToken)
    // mockRefreshTokens.push(refreshToken)
    this.sessionRepository.createSession({ refresh_token: refreshToken, token: accessToken })
    return { accessToken, refreshToken }
  }

  generateEmailVerificationToken(email: string) {
    const token = jwt.sign(email, process.env.SECRET_EMAIL_SENDER || '')
    return token
  }
  async validateToken({ token, refreshToken }: { token: string; refreshToken: string }) {
    try {
      jwt.verify(token, process.env.SECRET_TOKEN || '')
      jwt.verify(refreshToken, process.env.SECRET_REFRESH_TOKEN || '')

      const findedSession = this.sessionRepository.findSession({
        token,
        refresh_token: refreshToken
      })
      if (!findedSession) {
        console.log(new Date(), 'session not found')
        return { isValid: false }
      }

      console.log(new Date(), 'token no expired')
      const userAccount = jwt.decode(token) as jwt.JwtPayload
      return {
        role: userAccount.role as RoleType,
        isValid: true
      }
    } catch (err: any) {
      let error = err.message
      if (error.includes('expired')) {
        console.log(new Date(), 'expired token')
        const validRefreshToken = await this.checkSession({ refresh_token: refreshToken, token })
        if (validRefreshToken) {
          const userAccount = jwt.decode(token) as jwt.JwtPayload
          console.log(new Date(), 'valid session')
          const user: Account = {
            email: userAccount.email,
            role: userAccount.role as RoleType || 'USER' as RoleType,
            verified: userAccount.verified,
            accountId: userAccount.accountId,
            createdAt: userAccount.createdAt,
            updatedAt: userAccount.updatedAt,
            deletedAt: userAccount.deletedAt
          }
          const { accessToken, refreshToken } = this.signTokenAndRefreshToken(user)
          this.sessionRepository.createSession({ refresh_token: refreshToken, token: accessToken })
          return {
            role: user.role as RoleType || 'USER' as RoleType,
            isValid: true,
            newAccessToken: accessToken,
            newRefreshToken: refreshToken
          }
        } else {
          console.log(new Date(), 'invalid session')
          return {
            isValid: false,
            error: 'Invalid Session'
          }
        }
      } else {
        console.log(new Date(), 'invalid token')
        return {
          isValid: false,
          error: error
        }
      }
    }
  }

  async checkSession(session: Session): Promise<boolean> {
    const findedSession = await this.sessionRepository.findSession(session)
    console.log(new Date(), findedSession)
    if (!findedSession) return false
    console.log(new Date(), 'session exist')
    jwt.verify(session.refresh_token, process.env.SECRET_REFRESH_TOKEN || '', (err, _data) => {
      if (err) return false
    })
    console.log(new Date(), 'session exist and valid')
    this.sessionRepository.deleteSession(session)
    return true
  }
  signTokenAndRefreshToken(userAccount: Account) {
    const accessToken = jwt.sign(userAccount, process.env.SECRET_TOKEN || '', {
      expiresIn: '50000s'
    })
    const accountId = userAccount.accountId.toString()
    const refreshToken = jwt.sign({ accountId }, process.env.SECRET_REFRESH_TOKEN || '', {
      expiresIn: '500d'
    })
    return { accessToken, refreshToken }
  }

  logOutSession(session: Session) {
    const result = this.sessionRepository.deleteSession(session)
    if (!result) return false
    return true
  }

  // verifyToken (token: string) {
  //   try {
  //     const result = jwt.verify(token, process.env.SECRET_ACCOUNT_VERIFY || '')
  //     if(!token || !result) return false
  //     return true
  //   }
  //   catch(e) {
  //     console.log('error verifying token: ' + e?.toString())
  //     return false
  //   }
  // }
  verifyEmailToken(token: string) {
    try {
      const result = jwt.verify(token, process.env.SECRET_EMAIL || '')
      if (!token || !result) return false
      return true
    } catch (e) {
      console.log('error verifying token: ' + e?.toString())
      return false
    }
  }

  isAdmin(token: string) {
    const account =  jwt.decode(token) as JwtPayload
    const result = account.role === 'admin'?  true : false
    return result
  }
}
