import { PaymentInfo } from '../../Modules/payment/domain/PaymentInfo'

export class PaymentInfoMapper {
  static map(input: any): PaymentInfo {
    return {
      status: input?.status,
      date_approved: input?.date_approved,
      date_created: input?.date_created,
      description: input?.description,
      money_release_date: input?.money_release_date,
      money_release_schema: input?.money_release_schema,
      money_release_status: input?.money_release_status,
      operation_type: input?.operation_type,
      order: {
        id: input?.order?.id,
        type: input?.order?.type
      },
      payer: {
        identification: {
          number: input?.payer?.identification?.number,
          type: input?.payer?.identification?.type
        },
        entity_type: input?.payer?.entity_type,
        phone: {
          number: input?.payer?.phone?.number,
          extension: input?.payer?.phone?.extension,
          area_code: input?.payer?.phone?.area_code
        },
        last_name: input?.payer?.last_name,
        id: input?.payer?.id,
        type: input?.payer?.type,
        first_name: input?.payer?.first_name,
        email: input?.payer?.email
      },
      payment_method: {
        id: input?.payment_method?.id,
        issuer_id: input?.payment_method?.issuer_id,
        type: input?.payment_method?.type
      },
      payment_method_id: input?.payment_method_id,
      payment_type_id: input?.payment_type_id,
      refunds: input?.refunds || [],
      date_last_updated: input?.date_last_updated,
      transaction_details: {
        acquirer_reference: input?.transaction_details?.acquirer_reference,
        external_resource_url: input?.transaction_details?.external_resource_url,
        financial_institution: input?.transaction_details?.financial_institution,
        installment_amount: input?.transaction_details?.installment_amount || 0,
        net_received_amount: input?.transaction_details?.net_received_amount || 0,
        overpaid_amount: input?.transaction_details?.overpaid_amount || 0,
        payable_deferral_period: input?.transaction_details?.payable_deferral_period,
        payment_method_reference_id: input?.transaction_details?.payment_method_reference_id,
        total_paid_amount: input?.transaction_details?.total_paid_amount || 0
      }
    }
  }
}
