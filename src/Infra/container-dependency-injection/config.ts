export default {
  Server: {
    Express: 'Infra.Server.ServerExpress'
  },
  Repository: {
    ProcessOrderRepository: 'Infra.Repository.ProcessOrder'
  },
  Controller: {
    Order: {
      ProcessOrder: 'Infra.Controllers.Order.ProcessOrder'
    }
  }
}
