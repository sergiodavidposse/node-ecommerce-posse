import { ContainerBuilder, YamlFileLoader } from 'node-dependency-injection'
import containerNames from './config'
import path from 'path'
import config from '../Server/configs'

const container = new ContainerBuilder()

const loader = new YamlFileLoader(container)

const enviroment = config.ENVIROMENT.toLocaleLowerCase() || 'staging'

export const configPathYaml = path.join(__dirname, `application_${enviroment}.yaml`)

loader.load(configPathYaml)

export default container

export { containerNames }
