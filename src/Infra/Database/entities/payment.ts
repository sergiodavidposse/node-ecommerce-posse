import {
  Column,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn
} from 'typeorm'
import Company from './company'
import Currency from './currency'
import PaymentMethod from './payment_method'
import Order from './order'

@Entity('payment')
export default class Payment {
  @PrimaryColumn()
  public payment_id!: string

  @Column({ type: 'date' })
  public issue_date!: Date

  @Column()
  public status!: string

  @ManyToOne(() => Currency)
  public currency_id!: number

  @ManyToOne(() => PaymentMethod)
  public payment_method_id!: number

  @ManyToOne(() => Order)
  public order_id!: number

  @ManyToOne(() => Company)
  public company_id!: number

  @Column({ type: 'timestamp with time zone' })
  public created_at!: Date

  @Column({ nullable: true, type: 'timestamp with time zone' })
  public updated_at!: Date

  @DeleteDateColumn({ nullable: true, type: 'timestamp with time zone' })
  public deleted_at!: Date
}
