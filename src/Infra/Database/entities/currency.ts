import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('currency')
export default class Currency {
  @PrimaryGeneratedColumn()
  public currency_id!: number

  @Column()
  public name!: string
}
