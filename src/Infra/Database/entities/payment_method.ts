import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('payment_method')
export default class PaymentMethod {
  @PrimaryGeneratedColumn()
  public payment_method_id!: number

  @Column()
  public name!: string
}
