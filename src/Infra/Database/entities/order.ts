import { Column, DeleteDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import Account from './account'

@Entity('order')
export default class Order {
  @PrimaryGeneratedColumn()
  public order_id!: number

  @Column({ type: 'float' })
  public total_price!: number

  @ManyToOne(() => Account)
  public account_id!: number

  @Column({ type: 'timestamp with time zone' })
  public created_at!: Date

  @Column({ nullable: true, type: 'timestamp with time zone' })
  public updated_at!: Date

  @DeleteDateColumn({ nullable: true, type: 'timestamp with time zone' })
  public deleted_at!: Date
}
