import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm'
@Entity('account')
export default class Account {
  @PrimaryGeneratedColumn()
  public account_id!: number

  @Column()
  password!: string

  @Column({ unique: true })
  email!: string

  @Column()
  role!: string

  @Column({ nullable: true })
  verified!: boolean

  @Column({ type: 'timestamp with time zone' })
  created_at!: Date

  @Column({ nullable: true, type: 'timestamp with time zone' })
  updated_at!: Date

  @DeleteDateColumn({ nullable: true, type: 'timestamp with time zone' })
  deleted_at!: Date
}
