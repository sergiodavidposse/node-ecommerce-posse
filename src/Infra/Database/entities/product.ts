import { Column, DeleteDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('products')
export default class Product {
  @PrimaryGeneratedColumn()
  public product_id!: number

  @Column()
  public title!: string

  @Column()
  public description!: string

  @Column({ nullable: true })
  public full_description!: string

  @Column({ nullable: true })
  public url!: string

  @Column()
  public thumbnail_url!: string

  @Column({ type: 'float' })
  public unit_price!: number

  @Column()
  public type!: string

  @Column({ type: 'timestamp with time zone' })
  public created_at!: Date

  @Column({ nullable: true, type: 'timestamp with time zone' })
  public updated_at!: Date

  @DeleteDateColumn({ nullable: true, type: 'timestamp with time zone' })
  public deleted_at!: Date
}
