import { Column, DeleteDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('company')
export default class Company {
  @PrimaryGeneratedColumn()
  public company_id!: number

  @Column()
  public name!: string

  @Column()
  public address!: string

  @Column()
  public phone!: string

  @Column({ unique: true })
  public tax_id!: string

  @Column({ type: 'timestamp with time zone' })
  public created_at!: Date

  @Column({ type: 'timestamp with time zone' })
  public updated_at!: Date

  @DeleteDateColumn({ type: 'timestamp with time zone' })
  public deleted_at!: Date
}
