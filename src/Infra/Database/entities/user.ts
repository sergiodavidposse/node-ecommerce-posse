import { Column, DeleteDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm'
@Entity('user')
export default class User {
  @PrimaryGeneratedColumn()
  public user_id!: number

  @Column()
  name!: string

  @Column()
  lastname!: string

  @Column()
  address!: string

  @Column({ unique: true })
  tax_id!: string

  @Column()
  phone!: string

  @Column({ type: 'timestamp with time zone' })
  created_at!: Date

  @Column({ type: 'timestamp with time zone', nullable: true })
  updated_at!: Date

  @DeleteDateColumn({ type: 'timestamp with time zone', nullable: true })
  deleted_at!: Date

  @Column()
  account_id!: Number
}
