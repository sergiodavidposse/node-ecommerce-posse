import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm'
import Product from './product'
import Order from './order'

@Entity('order_product_detail')
export default class OrderProductDetail {
  @PrimaryColumn({ type: 'uuid', insert: false, select: false, update: false })
  public id!: never

  @ManyToOne(() => Order)
  public order_id!: number

  @ManyToOne(() => Product)
  public product_id!: number

  @Column()
  public quantity!: number
}
