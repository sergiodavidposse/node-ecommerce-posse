import { DataSource } from 'typeorm'
import * as dotenv from 'dotenv'
import { ConnectionAdapter } from './interface'
import path from 'path'

dotenv.config()

const port = Number(process.env.DB_PORT)
const dirPath = path.join(__dirname, '/entities/**/*.{ts,js}')

const AppDataSource = new DataSource({
  type: 'postgres',
  host: process.env.DB_HOST,
  port,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: true,
  logging: true,
  entities: [dirPath],
  migrations: [],
  subscribers: []
})

export default class Connection implements ConnectionAdapter {
  public async run() {
    await AppDataSource.initialize()
  }
  public getInstance() {
    return AppDataSource
  }
}
