import { Column, Entity, ObjectIdColumn, ObjectId } from 'typeorm'

@Entity('session')
export default class Session {
  @ObjectIdColumn()
  _id!: ObjectId

  @Column()
  public token!: string

  @Column()
  public refresh_token!: string
}
