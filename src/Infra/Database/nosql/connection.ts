import { DataSource } from 'typeorm'
import * as dotenv from 'dotenv'
import { ConnectionAdapter } from '../interface'
import path from 'path'

dotenv.config()

const dirPath = path.join(__dirname, '/entities/**/*.{ts,js}')
const mongoUri = `mongodb+srv://${process.env.DB_MONGO_USER}:${process.env.DB_MONGO_PASSWORD}${process.env.DB_MONGO_DOMAIN}`

const AppDataSource = new DataSource({
  type: 'mongodb',
  url: mongoUri,
  database: 'ecommerce-posse',
  useNewUrlParser: true,
  useUnifiedTopology: true,
  synchronize: true,
  logging: true,
  entities: [dirPath]
})

export default class Connection implements ConnectionAdapter {
  public async run() {
    await AppDataSource.initialize()
  }
  public getInstance() {
    return AppDataSource
  }
}
