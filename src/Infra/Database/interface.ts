import { DataSource } from 'typeorm'

export interface ConnectionAdapter {
  run: () => Promise<void>
  getInstance: () => DataSource
}
