import { AuthService as IAuthService } from '../../../Modules/account/interfaces'
import { Request, Response, NextFunction, CookieOptions } from 'express'
import { Middleware } from '../interfaces'
export class AuthMiddleware implements Middleware {
  constructor(private readonly authService: IAuthService) {}

  async handle(req: Request, res: Response, next: NextFunction): Promise<void> {
    const token = req.headers['authorization']
    const refreshToken = req.headers['refreshtoken'] as string
    if (!token || !refreshToken) {
      res.status(401).json({ message: 'No token provided' })
      return
    } else {
      const decoded = await this.authService.validateToken({ refreshToken, token })
      console.log(new Date(), decoded)
      if (decoded?.isValid === true) {
        const setCookieConfig: CookieOptions = {
          maxAge: 1000 * 60 * 60 * 24,
          httpOnly: true,
          secure: true,
          sameSite: 'lax'
        }
        if (decoded.newRefreshToken) {
          console.log(new Date(), 'new refreshToken')
          this.setCookies(res, 'refreshToken', decoded.newRefreshToken, { ...setCookieConfig })
        }
        if (decoded.newAccessToken) {
          console.log(new Date(), 'new accessToken')
          this.setCookies(res, 'accessToken', decoded.newAccessToken, { ...setCookieConfig })
        }
        next()
      } else {
        const resetCookieConfig: CookieOptions = {
          maxAge: 0,
          httpOnly: true,
          secure: true,
          sameSite: 'lax'
        }
        const errorMessage = decoded?.error || 'Invalid token'
        this.setCookies(res, 'accessToken', '', { ...resetCookieConfig })
        this.setCookies(res, 'refreshToken', '', { ...resetCookieConfig })
        res.status(401).json({ message: errorMessage })
      }
    }
  }

  private setCookies(res: Response, name: string, value: string, config: CookieOptions) {
    res.cookie(name, value, { ...config })
  }
}

export default AuthMiddleware
