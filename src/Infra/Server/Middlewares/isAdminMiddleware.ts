import { Middleware } from '../interfaces'
import { AuthService as IAuthService } from '../../../Modules/account/interfaces'

import { Request, Response, NextFunction } from 'express'
export class IsAdminMiddleware implements Middleware {
  constructor(private readonly authService: IAuthService) {}
  async handle(req: Request, res: Response, next: NextFunction): Promise<void> {
    const token = req.headers['authorization']
    if (!token) {
      res.status(401).send('No token provided')
    } else {
      const isAdmin = this.authService.isAdmin(token)
      if (isAdmin) {
        next()
      } else {
        res.status(403).send('You do not have privileges to see this')
      }
    }
  }
}
export default IsAdminMiddleware
