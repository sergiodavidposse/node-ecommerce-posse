import express from 'express'
import Router from 'express-promise-router'
import cors from 'cors'
import { Server } from '.'
import { registerRoutes } from '../Routes'
import configs from './configs'
import { ConnectionAdapter } from '../Database/interface'
import Connection from '../Database/nosql/connection'

export class ServerExpress implements Server {
  private app: express.Application

  constructor(
    private db: ConnectionAdapter,
    private dbNoSQL: ConnectionAdapter
  ) {
    this.app = express()
    this.boostrap()
  }

  private boostrap() {
    this.dbNoSQL
      .run()
      .then(() => {
        console.log('Conexión a la base de datos nosql establecida')
      })
      .catch((error) => console.log('Error al conectar a la base de datos nosql:', error))
    this.db
      .run()
      .then(() => {
        console.log('Conexión a la base de datos establecida')
      })
      .catch((error) => console.log('Error al conectar a la base de datos:', error))
    this.settings()
    this.routes()
  }

  private settings() {
    this.app.set('PORT', configs.PORT)
    this.app.use(express.json())

    const corsOpts = {
      origin: '*',
      methods: ['GET', 'POST'],
      allowedHeaders: ['Content-Type']
    }
    this.app.use(cors(corsOpts))
  }

  private routes() {
    const router = Router()
    this.app.use(router)
    registerRoutes(router)
  }

  public getApp() {
    return this.app
  }

  public start(): Promise<void> {
    return new Promise((resolve) => {
      this.app.listen(this.app.get('PORT'), () => {
        console.log(`Server is running on port ${this.app.get('PORT')}`)
      })
    })
  }
}
