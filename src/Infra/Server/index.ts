import { Application } from 'express'
// import { ConnectionSQLDatabase } from '../database/interface'

export interface Server {
  getApp(): Application
  start(): void
  // getDb(): ConnectionSQLDatabase
}
