import dotenv from 'dotenv'
dotenv.config()

export default {
  PORT: process.env.PORT || 3000,
  ENVIROMENT: process.env.ENV || 'STAGING',
  MP_ACCESSTOKEN: process.env.MP_ACCESSTOKEN
}
