import { Router } from 'express'
import path from 'path'
import glob from 'glob'

export const registerRoutes = (router: Router) => {
  const routes = glob.sync(path.join(__dirname, '/*.route.*'))
  routes.map((route) => register(route, router))
  return router
}

async function register(routePath: string, router: Router) {
  const route = require(routePath)
  return await route.register(router)
}

export default Router
