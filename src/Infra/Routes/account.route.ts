import { Request, Response, Router } from 'express'
import { Controller } from '../Controllers/interface'
import container, { containerNames } from '../container-dependency-injection'
import { Middleware } from '.././../Infra/Server/interfaces'

export const register = (router: Router) => {
  const authMiddleware: Middleware = container.get('Infra.Middlewares.AuthMiddleware') as Middleware
  const isAdminMiddleware: Middleware = container.get(
    'Infra.Middlewares.IsAdminMiddleware'
  ) as Middleware

  const createAccountController: Controller = container.get(
    'Infra.Controllers.Account.Create'
  ) as Controller
  const createAccountRoute = router.post(
    '/api/account/create-account',
    (req, res, next) => authMiddleware.handle(req, res, next),
    (req, res, next) => isAdminMiddleware.handle(req, res, next),
    async (req: Request, res: Response) => await createAccountController.run({ req, res })
  )

  const logInSessionController: Controller = container.get(
    'Infra.Controllers.Account.LogIn'
  ) as Controller
  const logInSessionRoute = router.post('/api/account/log-in', (req: Request, res: Response) =>
    logInSessionController.run({ req, res })
  )

  const logOutSessionController: Controller = container.get(
    'Infra.Controllers.Account.LogOut'
  ) as Controller
  const logOutSessionRoute = router.post('/api/account/log-out', (req: Request, res: Response) =>
    logOutSessionController.run({ req, res })
  )

  const verifyAccountController: Controller = container.get(
    'Infra.Controllers.Account.Verify'
  ) as Controller
  const verifyAccountRoute = router.get('/api/account/verify', (req: Request, res: Response) =>
    verifyAccountController.run({ req, res })
  )

  const sendVerificationEmailController: Controller = container.get(
    'Infra.Controllers.Account.SendEmail'
  ) as Controller
  const sendVerificationEmailControllerRoute = router.post(
    '/api/account/send-verification-email',
    (req: Request, res: Response) => sendVerificationEmailController.run({ req, res })
  )

  const getAllAccountsController: Controller = container.get(
    'Infra.Controllers.Account.GetAll'
  ) as Controller
  const getAllAccountsRoute = router.get('/api/account/get-all', (req: Request, res: Response) =>
    getAllAccountsController.run({ req, res })
  )
  // const saveRefreshTokenRoute = router.post('/api/account/refresh/save', async (req: Request, res: Response) => {
  //   const db = new Connection().getInstance()
  //   const refreshTokenRepository = db.getMongoRepository(RefreshTokenEntity)
  //   const {refresh_token, account_id} = req.body
  //   try {
  //     const refreshToken = new RefreshTokenEntity();
  //   refreshToken.refresh_token = refresh_token;
  //   refreshToken.account_id = account_id;

  //   const saveResult = await refreshTokenRepository.save(refreshToken);
  //     res.status(200).send('save');
  //   }
  //   catch(e) {
  //     console.error('Error creating account:', e);
  //     res.status(500).send({ error: e || 'Server error' });
  //   }
  // }
  // )

  return [
    {
      ...sendVerificationEmailControllerRoute,
      ...getAllAccountsRoute,
      ...logOutSessionRoute,
      ...createAccountRoute,
      ...logInSessionRoute,
      ...verifyAccountRoute
    }
  ]
}
