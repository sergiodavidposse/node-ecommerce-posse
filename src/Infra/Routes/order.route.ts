import { Request, Response, Router } from 'express'
import { Controller } from '../Controllers/interface'
import container, { containerNames } from '../container-dependency-injection'
import { Middleware } from '../Server/interfaces'

export const register = (router: Router) => {
  const processOrderController: Controller = container.get(
    'Infra.Controllers.Order.ProcessOrder'
  ) as Controller
  const authMiddleware: Middleware = container.get('Infra.Middlewares.AuthMiddleware') as Middleware
  const processOrderRouteV2 = router.post(
    '/api/order/process-order/v2',
    (req, res, next) => authMiddleware.handle(req, res, next),
    (req: Request, res: Response) => processOrderController.run({ req, res })
  )
  const processOrderRoute = router.post('/api/order/process-order', (req: Request, res: Response) =>
    processOrderController.run({ req, res })
  )
  const createOrderController: Controller = container.get(
    'Infra.Controllers.Order.CreateOrderController'
  ) as Controller
  const createOrderRoute = router.post(
    '/api/order/create-order',
    (req, res, next) => authMiddleware.handle(req, res, next),
    (req: Request, res: Response) => createOrderController.run({ req, res })
  )

  const getAllOrdersController: Controller = container.get(
    'Infra.Controllers.Order.GetAllOrdersController'
  ) as Controller
  const getAllOrdersRoute = router.get(
    '/api/order/get-all-orders',
    (req, res, next) => authMiddleware.handle(req, res, next),
    (req: Request, res: Response) => getAllOrdersController.run({ req, res })
  )

  const findOrderByAccountIdController: Controller = container.get(
    'Infra.Controllers.Order.FindOrderByAccountIdController'
  ) as Controller
  const findOrderByAccountIdRoute = router.post(
    '/api/order/find-order-by-account-id',
    (req, res, next) => authMiddleware.handle(req, res, next),
    (req: Request, res: Response) => findOrderByAccountIdController.run({ req, res })
  )

  return [
    {
      ...processOrderRoute,
      ...processOrderRouteV2,
      ...createOrderRoute,
      ...getAllOrdersRoute,
      ...findOrderByAccountIdRoute
    }
  ]
}
