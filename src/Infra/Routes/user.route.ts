import { Request, Response, Router } from 'express'
import { Controller } from '../Controllers/interface'
import container, { containerNames } from '../container-dependency-injection'

export const register = (router: Router) => {
  const createUserController: Controller = container.get(
    'Infra.Controllers.User.Create'
  ) as Controller
  const createUserRoute = router.post('/api/user/create-user', (req: Request, res: Response) =>
    createUserController.run({ req, res })
  )
  const editUserController: Controller = container.get(
    'Infra.Controllers.User.EditUserController'
  ) as Controller
  const editUserRoute = router.post('/api/user/edit-user', (req: Request, res: Response) =>
    editUserController.run({ req, res })
  )
  const findUserByAccountIdController: Controller = container.get(
    'Infra.Controllers.User.FindUserByAccountIdController'
  ) as Controller
  const findUserByAccountIdRoute = router.get(
    '/api/user/find-user-by-account-id',
    (req: Request, res: Response) => findUserByAccountIdController.run({ req, res })
  )

  return { ...createUserRoute }
}
