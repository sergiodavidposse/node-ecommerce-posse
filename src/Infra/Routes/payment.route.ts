import { Request, Response, Router } from 'express'
import { Controller } from '../Controllers/interface'
import container, { containerNames } from '../container-dependency-injection'
import { Middleware } from '../Server/interfaces'

export const register = (router: Router) => {
  const authMiddleware: Middleware = container.get('Infra.Middlewares.AuthMiddleware') as Middleware

  const createPaymentController: Controller = container.get(
    'Infra.Controllers.Payment.Create'
  ) as Controller
  const createPaymentRoute = router.post(
    '/api/payment/create-payment',
    (req, res, next) => authMiddleware.handle(req, res, next),
    (req: Request, res: Response) => createPaymentController.run({ req, res })
  )

  const findPaymentByOrderIdController: Controller = container.get(
    'Infra.Controllers.Payment.FindPaymentByOrderId'
  ) as Controller
  const findPaymentByOrderIdRoute = router.post(
    '/api/payment/find-by-order-id',
    (req, res, next) => authMiddleware.handle(req, res, next),
    (req: Request, res: Response) => findPaymentByOrderIdController.run({ req, res })
  )

  const updatePaymentStatusController: Controller = container.get(
    'Infra.Controllers.Payment.UpdatePaymentStatus'
  ) as Controller
  const updatePaymentStatusRoute = router.post(
    '/api/payment/update-payment-status',
    (req, res, next) => authMiddleware.handle(req, res, next),
    (req: Request, res: Response) => updatePaymentStatusController.run({ req, res })
  )

  const listenPaymentController: Controller = container.get(
    'Infra.Controllers.Payment.ListenPaymentController'
  ) as Controller
  const listenPaymentRoute = router.post(
    '/api/payment/listen-payment',
    (req: Request, res: Response) => listenPaymentController.run({ req, res })
  )

  return [
    {
      ...listenPaymentRoute,
      ...createPaymentRoute,
      ...findPaymentByOrderIdRoute,
      ...updatePaymentStatusRoute
    }
  ]
}
