import { Request, Response, Router } from 'express'
import { Controller } from '../Controllers/interface'
import container, { containerNames } from '../container-dependency-injection'

export const register = (router: Router) => {
  const createProductController: Controller = container.get(
    'Infra.Controllers.Product.CreateProductController'
  ) as Controller
  const createProductRoute = router.post(
    '/api/product/create-product',
    (req: Request, res: Response) => createProductController.run({ req, res })
  )

  const editProductController: Controller = container.get(
    'Infra.Controllers.Product.EditProductController'
  ) as Controller
  const editProductRoute = router.post('/api/product/edit-product', (req: Request, res: Response) =>
    editProductController.run({ req, res })
  )

  const deleteProductController: Controller = container.get(
    'Infra.Controllers.Product.DeleteProductController'
  ) as Controller
  const deleteProductRoute = router.post(
    '/api/product/delete-product',
    (req: Request, res: Response) => deleteProductController.run({ req, res })
  )

  const getAllProductsController: Controller = container.get(
    'Infra.Controllers.Product.GetAllProductsController'
  ) as Controller
  const getAllProductsRoute = router.get('/api/product/get-all', (req: Request, res: Response) =>
    getAllProductsController.run({ req, res })
  )

  const findProductByIdController: Controller = container.get(
    'Infra.Controllers.Product.FindProductByIdController'
  ) as Controller
  const findProductByIdRoute = router.post(
    '/api/product/find-product-by-id',
    (req: Request, res: Response) => findProductByIdController.run({ req, res })
  )

  return [
    {
      ...createProductRoute,
      ...deleteProductRoute,
      ...editProductRoute,
      ...getAllProductsRoute,
      ...findProductByIdRoute
    }
  ]
}
