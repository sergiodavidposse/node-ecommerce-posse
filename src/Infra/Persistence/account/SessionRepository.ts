import {
  SessionRepository as ISessionRepository,
  Session
} from '../../../Modules/account/interfaces'
import Connection from '../../Database/connection'
import { Session as SessionEntity } from '../../Database/nosql/entities'
import { Repository } from 'typeorm'

export class SessionRepository implements ISessionRepository {
  private readonly sessionRepository: Repository<SessionEntity>

  constructor(private database: Connection) {
    const db = this.database.getInstance()
    this.sessionRepository = db.getMongoRepository(SessionEntity)
  }

  public async createSession(session: Session): Promise<Session | string> {
    try {
      const sessionEntity = new SessionEntity()
      sessionEntity.refresh_token = session.refresh_token
      sessionEntity.token = session.token

      const saveResult = await this.sessionRepository.save(sessionEntity)
      return saveResult.refresh_token
    } catch (e) {
      let message = "Error can't save session"
      if (e) {
        message = e.toString()
      }
      console.error('Error creating session:', e)
      return 'Error: ' + message
    }
  }

  public async findSession(session: Session): Promise<Session | null> {
    try {
      const findedSession = await this.sessionRepository.findOne({
        where: { refresh_token: session.refresh_token, token: session.token }
      })
      if (findedSession) return findedSession
      return null
    } catch (e) {
      let message = "Error can't find valid session"
      if (e) {
        message = e.toString()
      }
      console.error('Error finding session:', message)
      return null
    }
  }

  public async deleteSession(session: Session): Promise<Boolean> {
    try {
      const result = await this.sessionRepository.delete({
        refresh_token: session.refresh_token,
        token: session.token
      })
      if (result.affected) {
        return result.affected > 0
      }
      return false
    } catch (e) {
      console.error('Error deleting session:', e)
      return false
    }
  }
}

export default SessionRepository
