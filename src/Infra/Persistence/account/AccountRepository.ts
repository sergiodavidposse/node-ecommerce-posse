import {
  AccountRepository as IAccountRepository,
  RoleType
} from '../../../Modules/account/interfaces'
import { CreateAccountRequest } from '../../../Modules/account/application/createAccountRequest'

import Connection from '../../Database/connection'
import { Account as AccountEntity } from '../../Database/entities'
import { Account } from '../../../Modules/account/interfaces'
import { Repository } from 'typeorm'

export class AccountRepository implements IAccountRepository {
  private readonly accountRepository: Repository<AccountEntity>

  constructor(private database: Connection) {
    const db = this.database.getInstance()
    this.accountRepository = db.getRepository(AccountEntity)
  }

  public async createAccount(account: CreateAccountRequest): Promise<Account | string> {
    try {
      const createdAccount = this.accountRepository.create(account)

      const responseSaveAccount = await this.accountRepository.save({
        ...createdAccount
      })
      return {
        email: responseSaveAccount.email,
        role: responseSaveAccount.role as RoleType,
        verified: responseSaveAccount.verified,
        createdAt: responseSaveAccount.created_at,
        updatedAt: responseSaveAccount.updated_at,
        deletedAt: responseSaveAccount.deleted_at,
        accountId: responseSaveAccount.account_id
      }
    } catch (e) {
      return 'Internal Server Error'
    }
  }

  public async findAccountByEmail(email: string): Promise<Account | null> {
    const findedAccount = await this.accountRepository.findOne({ where: { email } })
    if (!findedAccount) return null
    return {
      email: findedAccount.email,
      role: findedAccount.role as RoleType,
      verified: findedAccount.verified,
      createdAt: findedAccount.created_at,
      updatedAt: findedAccount.updated_at,
      deletedAt: findedAccount.deleted_at,
      accountId: findedAccount.account_id
    }
  }

  public async verifyAccount(account_id: number): Promise<boolean> {
    try {
      const result = await this.accountRepository.update(account_id, {
        verified: true,
        updated_at: new Date()
      })
      if (result.affected && result.affected > 0) {
        return true
      } else {
        return false
      }
    } catch (error) {
      console.error('Error verifying account:', error)
      return false
    }
  }

  async getAllAccounts() {
    const accounts = await this.accountRepository.find()
    const mappedAccount: Account[] = accounts.map((account) => {
      return {
        accountId: account.account_id,
        createdAt: account.created_at,
        deletedAt: account.deleted_at,
        email: account.email,
        role: account.role as RoleType,
        updatedAt: account.updated_at,
        verified: account.verified
      }
    })
    return mappedAccount
  }
}

export default AccountRepository
