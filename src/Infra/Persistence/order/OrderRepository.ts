import Connection from '../../Database/connection'
import { Order as OrderEntity } from '../../Database/entities'
import { Repository } from 'typeorm'
import { OrderRepository as IOrderRepository } from '../../../Modules/order/domain/OrderRepository'
import { Order } from '../../../Modules/order/domain/Order'
export class OrderRepository implements IOrderRepository {
  private readonly orderRepository: Repository<OrderEntity>
  constructor(private database: Connection) {
    const db = this.database.getInstance()
    this.orderRepository = db.getRepository(OrderEntity)
  }
  async createOrder(order: Order) {
    const mappedOrder = {
      total_price: order.totalPrice,
      account_id: order.accountId,
      created_at: new Date()
    }
    const newOrder = this.orderRepository.create(mappedOrder)
    const result = await this.orderRepository.save(newOrder)
    if (!result) return null
    return order
  }

  async getAll() {
    const orders = await this.orderRepository.find({ relations: ['account_id'] })
    const mappedOrders: Order[] = orders.map((order) => {
      return {
        totalPrice: order.total_price,
        accountId: order.account_id,
        createdAt: order.created_at,
        updated_at: order.updated_at,
        deleted_at: order.deleted_at
      }
    })
    return mappedOrders
  }

  public async findOrderByAccountId(accountId: number): Promise<Order[] | null> {
    const findedOrder = await this.orderRepository.find({
      where: { account_id: accountId },
      relations: ['account_id']
    })
    if (!findedOrder) return null
    const mappedOrder: Order[] = findedOrder.map((order) => {
      return {
        accountId: order.account_id,
        totalPrice: order.total_price
      }
    })
    return mappedOrder
  }
}
