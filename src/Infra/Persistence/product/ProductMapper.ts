import { Product as ProductDomain } from '../../../Modules/product/domain/Product'
import { Product as ProductEntity } from '../../Database/entities'

export class ProductMapper {
  public static toDomain(entity: ProductEntity): ProductDomain {
    return {
      title: entity.title,
      description: entity.description,
      fullDescription: entity.full_description,
      url: entity.url,
      thumbnailUrl: entity.thumbnail_url,
      unitPrice: entity.unit_price,
      type: entity.type
    }
  }

  public static toEntity(domain: ProductDomain): ProductEntity {
    const entity = new ProductEntity()
    if (domain.fullDescription) entity.full_description = domain.fullDescription
    if (domain.url) entity.url = domain.url
    if (domain.title) entity.title = domain.title
    if (domain.description) entity.description = domain.description
    if (domain.thumbnailUrl) entity.thumbnail_url = domain.thumbnailUrl
    if (domain.unitPrice) entity.unit_price = domain.unitPrice
    if (domain.type) entity.type = domain.type
    return entity
  }
}
