import { Product } from '../../../Modules/product/domain/Product'
import { ProductRepository as IProductRepository } from '../../../Modules/product/domain/ProductRepository'
import Connection from '../../../Infra/Database/connection'
import { Product as ProductEntity } from '../../Database/entities'
import { Repository } from 'typeorm'
import { ProductMapper } from './ProductMapper'
export class ProductRepository implements IProductRepository {
  private productRepository: Repository<ProductEntity>
  constructor(private database: Connection) {
    const db = database.getInstance()
    this.productRepository = db.getRepository(ProductEntity)
  }
  public async createProduct(product: Product): Promise<Product | null> {
    const entityFormatted = ProductMapper.toEntity(product)
    entityFormatted.created_at = new Date()
    const result = await this.productRepository.save(entityFormatted)
    if (!result) return null
    return product
  }

  public async editProduct(productId: number, updates: Product): Promise<Product | null> {
    const product = await this.productRepository.findOne({ where: { product_id: productId } })
    if (!product) return null
    const assign = ProductMapper.toEntity(updates)
    Object.assign(product, assign)
    const createdProduct = this.productRepository.create(product)
    createdProduct.updated_at = new Date()
    const updatedProduct = await this.productRepository.save(createdProduct)
    const domainFormatted: Product = ProductMapper.toDomain(updatedProduct)
    return domainFormatted
  }

  public async findById(productId: number): Promise<Product | null> {
    const findedProduct = await this.productRepository.findOne({ where: { product_id: productId } })
    if (!findedProduct) return null
    return ProductMapper.toDomain(findedProduct)
  }

  public async deleteProduct(productId: number): Promise<Boolean> {
    const product = await this.productRepository.findOne({ where: { product_id: productId } })
    if (!product) return false
    const createdProduct = this.productRepository.create(product)
    createdProduct.deleted_at = new Date()
    await this.productRepository.save(createdProduct)
    return true
  }

  public async getAllProducts(): Promise<Product[]> {
    const products = await this.productRepository.find()
    const mappedProduct = products.map((product) => {
      return ProductMapper.toDomain(product)
    })
    return mappedProduct
  }
}
