import { UserRepository as IUserRepository } from '../../../Modules/user/interfaces'
import { CreateUserRequest } from '../../../Modules/user/application/createUserRequest'

import Connection from '../../Database/connection'
import { User as UserEntity } from '../../Database/entities'
import { User } from '../../../Modules/user/interfaces'
import { DeepPartial, Repository } from 'typeorm'

export class UserRepository implements IUserRepository {
  private readonly userRepository: Repository<UserEntity>

  constructor(private database: Connection) {
    const db = this.database.getInstance()
    this.userRepository = db.getRepository(UserEntity)
  }

  public async createUser(request: CreateUserRequest): Promise<User | string> {
    const userBody = {
      ...request,
      created_at: new Date()
    }
    const createdUser = await this.userRepository.create(userBody)
    const responseSaveUser = await this.userRepository.save(createdUser)
    return responseSaveUser
  }
  async findByAccountId(accountId: number): Promise<User | null> {
    const findedUser = await this.userRepository.findOne({ where: { account_id: accountId } })
    if (!findedUser) return null
    const mappedUser: User = {
      address: findedUser.address,
      lastname: findedUser.lastname,
      name: findedUser.name,
      tax_id: findedUser.tax_id,
      phone: findedUser.phone
    }
    return mappedUser
  }

  public async editUser(accountId: number, updates: DeepPartial<UserEntity>): Promise<User | null> {
    const user = await this.userRepository.findOne({ where: { account_id: accountId } })
    if (!user) return null

    Object.assign(user, updates)

    const createdUser = this.userRepository.create(user)
    createdUser.updated_at = new Date()
    const updatedUser = await this.userRepository.save(createdUser)

    const mappedUser: User = {
      address: updatedUser.address,
      lastname: updatedUser.lastname,
      name: updatedUser.name,
      tax_id: updatedUser.tax_id,
      phone: updatedUser.phone
    }

    return mappedUser
  }
}

export default UserRepository
