import { Repository } from 'typeorm'
import { PaymentRepository as IPaymentRepository } from '../../../Modules/payment/domain/PaymentRepository'
import { Company, Payment as PaymentEntity } from '../../Database/entities'
import Connection from '../../../Infra/Database/connection'
import { Payment } from '../../../Modules/payment/domain/Payment'
import { PaymentWithRelations } from '../../../Modules/payment/domain/PaymentWithRelations'
import { PaymentStatus } from '../../../Modules/payment/domain/PaymentInfo'

export class PaymentRepository implements IPaymentRepository {
  private readonly paymentRepository: Repository<PaymentEntity>

  constructor(private readonly database: Connection) {
    const db = database.getInstance()
    this.paymentRepository = db.getRepository(PaymentEntity)
  }

  async createPayment(paymentId: string, orderId: number) {
    const newPayment = {
      payment_id: paymentId,
      company_id: 4, //hardcoded WIP
      created_at: new Date(),
      issue_date: new Date(),
      order_id: orderId,
      payment_method_id: 1, //hardcoded WIP
      status: 'created',
      currency_id: 1 //hardcoded WIP
    }
    // const newPaymentCreated = this.paymentRepository.create(newPayment)
    const result = await this.paymentRepository.save({ ...newPayment })
    if (!result) return null
    return this.mapPayment(result)
  }
  mapPayment(payment: PaymentEntity): Payment {
    return {
      paymentId: payment.payment_id,
      companyId: payment.company_id,
      issueDate: payment.issue_date,
      paymentMethodId: payment.payment_method_id,
      status: payment.status as PaymentStatus,
      currencyId: payment.currency_id,
      orderId: payment.order_id
    }
  }

  async findPaymentByOrderId(orderId: number): Promise<PaymentWithRelations | null> {
    const findedPayment: any = await this.paymentRepository.findOne({
      where: { order_id: orderId },
      relations: ['order_id', 'company_id', 'currency_id', 'payment_method_id']
    })
    const mappedPayment: PaymentWithRelations = {
      order: findedPayment?.order,
      company: findedPayment?.company,
      currency: findedPayment?.currency,
      issueDate: findedPayment.issue_date,
      paymentMethod: findedPayment.payment_method,
      status: findedPayment.status,
      paymentId: findedPayment.payment_id
    }
    if (!findedPayment) return null
    return mappedPayment
  }

  async updatePaymentStatus(paymentId: string, status: PaymentStatus): Promise<Boolean> {
    const payment_id = paymentId
    const result = await this.paymentRepository.update(payment_id, {
      status,
      updated_at: new Date()
    })
    if (result.affected && result.affected > 0) {
      return true
    } else {
      return false
    }
  }
}
