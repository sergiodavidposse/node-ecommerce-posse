import CreateAccount from '../../../Modules/account/application/CreateAccount'
import { Request, Response } from 'express'

export default class CreateAccountController {
  constructor(private readonly createAccount: CreateAccount) {}

  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const { email, password } = req.body
      const response = await this.createAccount.execute({ email, password })
      typeof response === 'string' && res.status(500).send(response)
      response && res.status(200).send(response)
    } catch (e) {
      console.error('Error creating account:', e)
      res.status(500).send({ error: e || 'Server error' })
    }
  }
}
