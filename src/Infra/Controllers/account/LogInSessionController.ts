import LogInSession from '../../../Modules/account/application/LogInSession'
import { Request, Response } from 'express'

export default class LogInSessionController {
  constructor(private readonly logInSession: LogInSession) {}

  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const { email, password } = req.body
      const response = await this.logInSession.execute({ email, password })
      if (response) {
        const { refreshToken, accessToken } = response
        res.cookie('accessToken', accessToken, {
          maxAge: 1000 * 60 * 60 * 24,
          httpOnly: true,
          secure: true,
          sameSite: 'lax'
        })
        res.cookie('refreshToken', refreshToken, {
          maxAge: 1000 * 60 * 60 * 24,
          httpOnly: true,
          secure: true,
          sameSite: 'lax'
        })
        response && res.status(200).send(response)
      }
      !response && res.status(400).send('error in credentials')
    } catch (e) {
      console.error('Error logging session:', e)
      res.status(500).send({ error: e || 'Server error' })
    }
  }
}
