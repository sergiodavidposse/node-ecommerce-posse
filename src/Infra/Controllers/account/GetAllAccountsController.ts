import GetAllAccounts from '../../../Modules/account/application/GetAllAccounts'
import { Request, Response } from 'express'

export class GetAllAccountsController {
  constructor(private getAllAccounts: GetAllAccounts) {}
  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.getAllAccounts.execute()
      if (!result) {
        return res.status(400).json({ message: 'Error retrieving accounts' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
