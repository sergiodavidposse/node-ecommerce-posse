import LogOutSession from '../../../Modules/account/application/LogOutSession'
import { Request, Response } from 'express'

export default class LogOutSessionController {
  constructor(private readonly logOutSession: LogOutSession) {}

  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const token = req.headers['authorization']
      const refreshToken = req.headers['refreshtoken'] as string
      if (!token || !refreshToken) return 'error'
      const response = await this.logOutSession.logOut({ refresh_token: refreshToken, token })
      if (response) {
        res.cookie('accessToken', '', {
          maxAge: 1000 * 60 * 60 * 24,
          httpOnly: true,
          secure: true,
          sameSite: 'lax'
        })
        res.cookie('refreshToken', '', {
          maxAge: 1000 * 60 * 60 * 24,
          httpOnly: true,
          secure: true,
          sameSite: 'lax'
        })
        response && res.status(200).send(response)
      }
      !response && res.status(400).send('error in deleting session')
    } catch (e) {
      console.error('Error ending session:', e)
      res.status(500).send({ error: e || 'Server error' })
    }
  }
}
