import VerifyAccount from '../../../Modules/account/application/VerifyAccount'
import { Request, Response } from 'express'

export default class VerifyAccountController {
  constructor(private readonly verifyAccount: VerifyAccount) {}

  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const { email, token } = req.query
      if (typeof email !== 'string' || typeof token !== 'string') {
        return res.status(400).send({ error: 'Email or token is missing or invalid' })
      }
      const response = await this.verifyAccount.execute({ email, token })
      !response && res.status(400).send(response)
      response && res.status(200).send(response)
    } catch (e) {
      console.error('Error verifying account:', e)
      res.status(500).send({ error: e || 'Server error' })
    }
  }
}
