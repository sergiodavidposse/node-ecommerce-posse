import { SendEmail } from '../../../Modules/account/application/SendEmail'
import { Request, Response } from 'express'

export class SendEmailController {
  constructor(private SendEmail: SendEmail) {}
  public async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.SendEmail.execute(req.body)
      if (!result) return res.status(400).json({ message: 'Error sending email' })
      return res.status(200).json({ message: 'Succefully sended' })
    } catch (e: any) {
      return res.status(500).json({ error: e.message, message: 'Server error' })
    }
  }
}
