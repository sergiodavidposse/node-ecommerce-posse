import { FindProductById } from '../../../Modules/product/application/FindProductById'
import { Request, Response } from 'express'

export class FindProductByIdController {
  constructor(private FindProductById: FindProductById) {}
  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.FindProductById.execute(req.body.productId)
      if (!result) {
        return res.status(400).json({ message: 'Error saving product' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
