import { EditProduct } from '../../../Modules/product/application/EditProduct'
import { Request, Response } from 'express'

export class EditProductController {
  constructor(private EditProduct: EditProduct) {}
  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.EditProduct.execute(req.body.productId, req.body.updates)
      if (!result) {
        return res.status(400).json({ message: 'Error updating product' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
