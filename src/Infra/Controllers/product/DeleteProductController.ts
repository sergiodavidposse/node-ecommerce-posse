import { DeleteProduct } from '../../../Modules/product/application/DeleteProduct'
import { Request, Response } from 'express'

export class DeleteProductController {
  constructor(private DeleteProduct: DeleteProduct) {}
  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.DeleteProduct.execute(req.body.productId)
      if (!result) {
        return res.status(400).json({ message: 'Error deleting product' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
