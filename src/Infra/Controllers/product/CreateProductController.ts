import { CreateProduct } from '../../../Modules/product/application/CreateProduct'
import { Request, Response } from 'express'

export class CreateProductController {
  constructor(private CreateProduct: CreateProduct) {}
  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.CreateProduct.execute(req.body)
      if (!result) {
        return res.status(400).json({ message: 'Error saving product' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
