import { GetAllProducts } from '../../../Modules/product/application/GetAllProducts'
import { Request, Response } from 'express'

export class GetAllProductsController {
  constructor(private GetAllProducts: GetAllProducts) {}
  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.GetAllProducts.execute()
      if (!result) {
        return res.status(400).json({ message: 'Error saving product' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
