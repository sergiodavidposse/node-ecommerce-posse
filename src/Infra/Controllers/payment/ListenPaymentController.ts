import { CreatePayment } from '../../../Modules/payment/application/CreatePayment'
import { ListenPayment } from '../../../Modules/payment/application/ListenPayment'
import { Request, Response } from 'express'

export class ListenPaymentController {
  constructor(
    private listenPayment: ListenPayment,
    private createPayment: CreatePayment
  ) {}
  public async run({ req, res }: { req: Request; res: Response }) {
    try {
      // crear order y order details cuando llegue la notificacion de mp webhook payment
      // const result = await this.createPayment.execute(req.body.orderId)
      const paymentCreated = await this.createPayment.execute(req.body.data.id, 26) //hardcoded orderid existente
      if (!paymentCreated) return res.status(400).send('Error saving payment')
      const result = await this.listenPayment.execute(req.body.data.id)
      if (!result) return res.status(400).send(result)
      return res.status(200).send(result)
    } catch (e: any) {
      return res.status(500).send({ error: e?.message, message: 'Server Error' })
    }
  }
}
