import { FindPaymentByOrderId } from '../../../Modules/payment/application/FindPaymentByOrderId'
import { Request, Response } from 'express'

export class FindPaymentByOrderIdController {
  constructor(private findPaymentByOrderId: FindPaymentByOrderId) {}
  public async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.findPaymentByOrderId.execute(req.body.orderId)
      if (!result) {
        return res.status(400).json({ message: 'Error finding payment' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      res.status(500).send({ message: 'Server error', error: e?.message })
    }
  }
}
