import { CreatePayment } from '../../../Modules/payment/application/CreatePayment'
import { Request, Response } from 'express'

export class CreatePaymentController {
  constructor(private CreatePayment: CreatePayment) {}
  async run({ req, res }: { req: Request; res: Response }) {
    try {
      // const result = await this.CreatePayment.execute(req.body.orderId)
      const result = await this.CreatePayment.execute(req.body.paymentId, 26) //hardcoded for testing
      if (!result) {
        return res.status(400).json({ message: 'Error saving payment' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
