import { UpdatePaymentStatus } from '../../../Modules/payment/application/UpdatePaymentStatus'
import { Request, Response } from 'express'

export class UpdatePaymentStatusController {
  constructor(private updatePaymentStatus: UpdatePaymentStatus) {}
  public async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.updatePaymentStatus.execute(req.body.paymentId, req.body.status)
      if (!result) {
        return res.status(400).json({ message: 'Error updating payment status' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      res.status(500).send({ message: 'Server error', error: e?.message })
    }
  }
}
