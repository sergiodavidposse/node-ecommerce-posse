import { FindUserByAccountId } from '../../../Modules/user/application/FindUserByAccountId'
import { Request, Response } from 'express'

export class FindUserByAccountIdController {
  constructor(private findUserByAccountId: FindUserByAccountId) {}
  public async run({ req, res }: { req: Request; res: Response }) {
    const { accountId } = req.query
    if (typeof accountId !== 'string') {
      return res.status(400).send({ error: 'accountId is missing or invalid' })
    }
    try {
      const result = await this.findUserByAccountId.execute(parseInt(accountId, 10))
      if (!result) return res.status(400).send('Error retrieving user information')
      return res.status(200).send(result)
    } catch (e: any) {
      return res.status(500).json({ error: e, message: 'Server error' })
    }
  }
}
