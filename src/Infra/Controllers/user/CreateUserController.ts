import CreateUser from '../../../Modules/user/application/CreateUser'
import { Request, Response } from 'express'

export default class CreateUserController {
  constructor(private readonly userCreate: CreateUser) {}

  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const response = await this.userCreate.execute({ ...req.body })
      response && res.status(200).send(response)
    } catch (e: any) {
      res.status(400).send({
        message: e.message || 'An unexpected error occurred',
        stack: e.stack
      })
    }
  }
}
