import { EditUser } from '../../../Modules/user/application/EditUser'
import { Request, Response } from 'express'

export class EditUserController {
  constructor(private editUser: EditUser) {}
  public async run({ req, res }: { req: Request; res: Response }) {
    const { accountId, user } = req.body

    try {
      const result = await this.editUser.execute(accountId, user)
      if (!result) return res.status(400).send('Error updating user information')
      return res.status(200).send(result)
    } catch (e: any) {
      return res.status(500).json({ error: e?.message, message: 'Server error' })
    }
  }
}
