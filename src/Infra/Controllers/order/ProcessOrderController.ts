import { Request, Response } from 'express'
import { Controller } from '../interface'
import ProcessOrder from '../../../Modules/order/application/ProcessOrder'
export class ProcessOrderController implements Controller {
  constructor(private readonly instanceProcessOrder: ProcessOrder) {}

  async run({ req, res }: { req: Request; res: Response }) {
    try {
      if (req.body.length < 1) {
        throw new Error('can not send empty')
      }

      const response = await this.instanceProcessOrder.run({ ...req.body })
      response && res.status(200).send(response)
    } catch (e) {
      console.error('Error in processing order:', e)
      res.status(400).send({ error: e || 'Server error' })
    }
  }
}
