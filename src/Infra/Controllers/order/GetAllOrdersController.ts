import GetAllOrders from '../../../Modules/order/application/GetAllOrders'
import { Request, Response } from 'express'

export class GetAllOrdersController {
  constructor(private getAllOrders: GetAllOrders) {}
  async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.getAllOrders.execute()
      if (!result) {
        return res.status(400).json({ message: 'Error retrieving orders' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
