import { CreateOrder } from '../../../Modules/order/application/CreateOrder'
import { Request, Response } from 'express'

export class CreateOrderController {
  constructor(private CreateOrder: CreateOrder) {}
  async run({ req, res }: { req: Request; res: Response }) {
    const { totalPrice, accountId } = req.body
    try {
      const result = await this.CreateOrder.execute({ accountId, totalPrice })
      if (!result) {
        return res.status(400).json({ message: 'Error saving order' })
      }
      return res.status(200).json({ message: 'Succefully' })
    } catch (e: any) {
      return res.status(500).json({ message: 'Server error', error: e?.message })
    }
  }
}
