import { FindOrderByAccountId } from '../../../Modules/order/application/FindOrderByAccountId'
import { Request, Response } from 'express'

export class FindOrderByAccountIdController {
  constructor(private findOrderByAccountId: FindOrderByAccountId) {}
  public async run({ req, res }: { req: Request; res: Response }) {
    try {
      const result = await this.findOrderByAccountId.execute(req.body.accountId)
      if (!result) {
        return res.status(400).json({ message: 'Error finding order' })
      }
      return res.status(200).json(result)
    } catch (e: any) {
      res.status(500).send({ message: 'Server error', error: e?.message })
    }
  }
}
