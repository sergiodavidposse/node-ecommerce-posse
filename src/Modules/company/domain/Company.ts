export interface Company {
  company_id: number
  name: string
  address: string
  phone: string
  taxId: string
}
