import { UserRepository } from '../interfaces'
import { CreateUserRequest } from './createUserRequest'

export class CreateUser {
  constructor(private repository: UserRepository) {}

  public async execute(request: CreateUserRequest) {
    const { name, lastname, tax_id, address, account_id } = request

    if (!account_id) {
      throw new Error('error getting account_id')
    }

    if (!name || !lastname || !tax_id || !address) {
      throw new Error('name, lastname, tax_id, address are mandatory')
    }
    const newUser = await this.repository.createUser({ ...request })
    return newUser
  }
}

export default CreateUser
