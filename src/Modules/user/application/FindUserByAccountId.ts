import { UserRepository } from '../interfaces'

export class FindUserByAccountId {
  constructor(private userRepository: UserRepository) {}
  public async execute(accountId: number) {
    const result = this.userRepository.findByAccountId(accountId)
    return result
  }
}
