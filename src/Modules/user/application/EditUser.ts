import { User, UserRepository } from '../interfaces'

export class EditUser {
  constructor(private userRepository: UserRepository) {}
  public async execute(accountId: number, user: User) {
    if (!accountId) throw new Error('accountId is mandatory')
    if (!user || Object.keys(user).length === 0) {
      throw new Error(
        'User object is invalid. It cannot be null and must have all required properties.'
      )
    }
    const result = this.userRepository.editUser(accountId, user)
    return result
  }
}
