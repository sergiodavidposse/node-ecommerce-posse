export interface CreateUserRequest {
  account_id: number
  name: string
  lastname: string
  address: string
  tax_id: string
  phone: string
}
