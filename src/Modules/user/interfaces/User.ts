export interface User {
  name: string
  lastname: string
  address: string
  tax_id: string
  phone?: string
}
