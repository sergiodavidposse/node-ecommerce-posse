import { CreateUserRequest } from '../application/createUserRequest'
import { User } from './User'

export interface UserRepository {
  createUser(user: CreateUserRequest): Promise<User | string>
  findByAccountId(accountId: number): Promise<User | null>
  editUser(accountId: number, user: User): Promise<User | null>
}
