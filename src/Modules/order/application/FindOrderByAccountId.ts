import { OrderRepository } from '../domain/OrderRepository'

export class FindOrderByAccountId {
  constructor(private readonly orderRepository: OrderRepository) {}
  public async execute(accountId: number) {
    const result = await this.orderRepository.findOrderByAccountId(accountId)
    return result
  }
}
