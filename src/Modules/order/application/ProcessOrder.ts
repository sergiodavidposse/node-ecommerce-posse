import { ProcessOrder as IProcessOrder } from '../domain/ProcessOrder'
import { ProcessOrderService } from '../domain/ProcessOrderService'
import { MercadoPagoPreference } from '../domain/MercadoPagoPreference'

class ProcessOrder implements IProcessOrder {
  constructor(private readonly processOrderService: ProcessOrderService) {}
  public async run(purchaseData: MercadoPagoPreference[]) {
    const preferenceArray = Object.values(purchaseData)
    preferenceArray.forEach((preference) => {
      if (!preference.description) throw new Error('description is mandatory')
      if (!preference.price) throw new Error('price is mandatory')
      if (!preference.name) throw new Error('name is mandatory')
    })
    const response = await this.processOrderService.processOrder(preferenceArray)
    if (response) return response
    return null
  }
}

export default ProcessOrder
