import { OrderRepository } from '../domain/OrderRepository'

export class GetAllOrders {
  constructor(private readonly orderRepository: OrderRepository) {}
  public async execute() {
    const result = await this.orderRepository.getAll()
    return result
  }
}

export default GetAllOrders
