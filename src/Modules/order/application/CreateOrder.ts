import { Order } from '../domain/Order'
import { OrderRepository } from '../domain/OrderRepository'

export class CreateOrder {
  constructor(private orderRepository: OrderRepository) {}
  public async execute(order: Order) {
    if (!order.totalPrice) throw new Error('total price is mandatory')
    if (!order.accountId) throw new Error('account id is mandatory')

    const result = await this.orderRepository.createOrder(order)
    if (!result) return null
    return order
  }
}
