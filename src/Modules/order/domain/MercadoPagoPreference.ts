export interface MercadoPagoPreference {
  id: string
  name: string
  price: number
  quantity: number
  description: string
  thumbnail: string
  currency: string
}
