import { Order } from './Order'

export interface OrderRepository {
  createOrder: (order: Order) => Promise<Order | null>
  getAll: () => Promise<Order[]>
  findOrderByAccountId: (accountId: number) => Promise<Order[] | null>
}
