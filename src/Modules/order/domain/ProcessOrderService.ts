import { ProcessOrderResponse } from './ProcessOrderResponse'
import { MercadoPagoPreference } from './MercadoPagoPreference'
import { PaymentInfo } from '../../../Modules/payment/domain/PaymentInfo'

export interface ProcessOrderService {
  processOrder: (data: MercadoPagoPreference[]) => Promise<ProcessOrderResponse | null>
  getPaymentInfo: (paymentId: string) => Promise<PaymentInfo | null>
  getPaymentInfoAPI: (paymentId: string) => Promise<any>
}
