export interface Order {
  totalPrice: number
  accountId: number
}
