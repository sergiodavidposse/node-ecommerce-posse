import { ProcessOrderResponse } from './ProcessOrderResponse'
import { MercadoPagoPreference } from './MercadoPagoPreference'

export interface ProcessOrder {
  run(purchaseData: MercadoPagoPreference[]): Promise<ProcessOrderResponse | null>
}
