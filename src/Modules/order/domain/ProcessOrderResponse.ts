export interface ProcessOrderResponse {
  paymentUrl: string
  paymentSandboxUrl: string
  preferenceId: string
}
