import { Product } from './Product'

export interface ProductRepository {
  createProduct(product: Product): Promise<Product | null>
  getAllProducts(): Promise<Product[]>
  findById(productId: number): Promise<Product | null>
  deleteProduct(productId: number): Promise<Boolean>
  editProduct(productId: number, updates: Product): Promise<Product | null>
}
