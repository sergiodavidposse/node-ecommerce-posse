export interface Product {
  title: string
  description: string
  fullDescription: string
  url: string
  thumbnailUrl: string
  unitPrice: number
  type: string
}
