import { ProductRepository } from '../domain/ProductRepository'

export class FindProductById {
  constructor(private productRepository: ProductRepository) {}
  public async execute(productId: number) {
    return await this.productRepository.findById(productId)
  }
}
