import { ProductRepository } from '../domain/ProductRepository'

export class DeleteProduct {
  constructor(private productRepository: ProductRepository) {}
  public async execute(productId: number) {
    return await this.productRepository.deleteProduct(productId)
  }
}
