import { ProductRepository } from '../domain/ProductRepository'

export class GetAllProducts {
  constructor(private productRepository: ProductRepository) {}
  public async execute() {
    return await this.productRepository.getAllProducts()
  }
}
