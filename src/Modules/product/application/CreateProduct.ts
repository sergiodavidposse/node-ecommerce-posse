import { Product } from '../domain/Product'
import { ProductRepository } from '../domain/ProductRepository'

export class CreateProduct {
  constructor(private productRepository: ProductRepository) {}
  public async execute(product: Product) {
    return await this.productRepository.createProduct(product)
  }
}
