import { Product } from '../domain/Product'
import { ProductRepository } from '../domain/ProductRepository'

export class EditProduct {
  constructor(private productRepository: ProductRepository) {}
  public async execute(productId: number, updates: Product) {
    return await this.productRepository.editProduct(productId, updates)
  }
}
