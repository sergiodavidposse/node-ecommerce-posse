import { AccountRepository, AuthService } from '../interfaces'

export class VerifyAccount {
  constructor(
    private accountRepository: AccountRepository,
    private authService: AuthService
  ) {}

  async execute({ email, token }: { email: string; token: string }) {
    const validateToken = this.authService.verifyEmailToken(token)
    if (!validateToken) return false
    const account = await this.accountRepository.findAccountByEmail(email)
    if (!account) return false
    const result = this.accountRepository.verifyAccount(account.accountId)
    if (!result) return false
    return true
  }
}

export default VerifyAccount
