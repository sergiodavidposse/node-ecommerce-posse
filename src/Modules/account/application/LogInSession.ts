import { Account, AccountRepository } from '../interfaces'
import { AuthService } from '../interfaces/AuthService'
import { LogInSessionRequest } from './logInSessionRequest'

export class LogInSession {
  constructor(
    private repository: AccountRepository,
    private service: AuthService
  ) {}
  // util bcrypt o rsa
  // util generate token

  public async execute(request: LogInSessionRequest) {
    const { email, password } = request
    if (!email || !password) {
      throw new Error('name email and password are mandatory')
    }
    const account = await this.repository.findAccountByEmail(email)
    if (!account) return null
    const { accessToken, refreshToken } = this.service.generateToken(account as Account)
    return { accessToken, refreshToken }
  }
}

export default LogInSession
