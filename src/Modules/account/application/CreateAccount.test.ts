import { CreateAccount } from './CreateAccount'
import { AccountRepository, RoleType } from '../interfaces'
import { CreateAccountRequest } from './createAccountRequest'

// Mock del repositorio
const mockAccountRepository: jest.Mocked<AccountRepository> = {
  createAccount: jest.fn(),
  findAccountByEmail: jest.fn(),
  verifyAccount: jest.fn(),
  getAllAccounts: jest.fn()
}

describe('CreateAccount', () => {
  let createAccount: CreateAccount

  beforeEach(() => {
    createAccount = new CreateAccount(mockAccountRepository)
  })

  it('should return an error if email or password is missing', async () => {
    const request: CreateAccountRequest = { email: '', password: '' }
    const result = await createAccount.execute(request)
    expect(result).toEqual(new Error('name email and password are mandatory'))
  })

  it('should create an account successfully', async () => {
    const today = new Date()
    const defaultValues = {
      verified: false,
      created_at: today,
      role: 'user' as RoleType
    }
    const request: CreateAccountRequest = { email: 'sergio@sergio.test', password: '123456' }
    mockAccountRepository.createAccount.mockResolvedValue({
      email: request.email,
      role: defaultValues.role,
      verified: defaultValues.verified,
      accountId: 1, // mocked
      createdAt: defaultValues.created_at
    })
    const result = await createAccount.execute(request)
    expect(mockAccountRepository.createAccount).toHaveBeenCalledWith({
      ...request,
      ...defaultValues
    })
    expect(result).toEqual({
      role: 'user',
      verified: false,
      email: request.email,
      accountId: 1,
      createdAt: today
    })
  })
})
