export interface LogInSessionRequest {
  email: string
  password: string
}
