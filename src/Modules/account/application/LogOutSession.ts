import { AuthService, Session, SessionRepository } from '../interfaces'

export class LogOutSession {
  constructor(private authService: AuthService) {}

  async logOut(session: Session) {
    const result = await this.authService.logOutSession(session)
    if (!result) return false
    return true
  }
}

export default LogOutSession
