import { AccountRepository, RoleType } from '../interfaces'
import { CreateAccountRequest } from './createAccountRequest'

export class CreateAccount {
  constructor(private repository: AccountRepository) {}

  public async execute(request: CreateAccountRequest) {
    const { email, password } = request
    const createAccountInput = this.setDefaultValues(request)
    if (!email || !password) {
      return new Error('name email and password are mandatory')
    }
    const newAccount = await this.repository.createAccount({ ...createAccountInput })
    return newAccount
  }

  setDefaultValues(request: CreateAccountRequest) {
    return {
      email: request.email,
      password: request.password,
      verified: false,
      created_at: new Date(),
      role: 'user' as RoleType
    }
  }
}

export default CreateAccount
