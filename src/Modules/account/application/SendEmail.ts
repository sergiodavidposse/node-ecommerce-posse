import { AuthService } from '../interfaces'
import { EmailService, EmailServiceRequest } from '../interfaces/EmailService'

export class SendEmail {
  constructor(
    private emailService: EmailService,
    private authService: AuthService
  ) {}
  public async execute(request: EmailServiceRequest) {
    const token = this.authService.generateEmailVerificationToken(request.to)
    const params = new URLSearchParams({
      token,
      email: request.to
    })
    request.text = `${process.env.BACKEND_URL}/api/account/verify?${params.toString()}`
    const result = await this.emailService.sendEmail(request)
    return result
  }
}
