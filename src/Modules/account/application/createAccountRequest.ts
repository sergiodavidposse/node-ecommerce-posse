import { RoleType } from "../interfaces"

export interface CreateAccountRequest {
  email: string
  password: string
  // role: RoleType,
  // verified: boolean,
  // createdAt: Date
}
