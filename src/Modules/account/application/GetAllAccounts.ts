import { AccountRepository } from '../interfaces/AccountRepository'

export class GetAllAccounts {
  constructor(private readonly accountRepository: AccountRepository) {}
  public async execute() {
    const result = await this.accountRepository.getAllAccounts()
    return result
  }
}

export default GetAllAccounts
