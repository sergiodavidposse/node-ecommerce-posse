export interface Session {
  token: string
  refresh_token: string
}
