import { RoleType } from "./AuthService"

export interface Account {
  email: string
  role: RoleType
  verified: boolean
  accountId: number
  createdAt: Date
  updatedAt?: Date
  deletedAt?: Date
}
