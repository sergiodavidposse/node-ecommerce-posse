import { CreateAccountRequest } from '../application/createAccountRequest'
import { Account } from './Account'

export interface AccountRepository {
  createAccount(account: CreateAccountRequest): Promise<Account | string>
  findAccountByEmail(email: string): Promise<Account | null>
  verifyAccount(account_id: number): Promise<Boolean>
  getAllAccounts(): Promise<Account[]>
}
