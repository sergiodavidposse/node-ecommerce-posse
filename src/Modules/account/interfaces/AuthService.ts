import { Account } from './Account'
import { Session } from './Session'

export type RoleType = 'user' | 'admin'
interface ValidateTokenResponse {
  isValid: boolean
  role?: RoleType
  newAccessToken?: string
  newRefreshToken?: string
  error?: string
}
interface ValidateTokenRequest {
  token: string
  refreshToken: string
}
export interface AuthService {
  validateToken: (request: ValidateTokenRequest) => Promise<ValidateTokenResponse | undefined>
  generateToken: (account: Account) => { accessToken: string; refreshToken: string }
  generateEmailVerificationToken: (email: string) => string
  logOutSession: (session: Session) => Boolean
  // verifyToken: (token: string) => Boolean
  verifyEmailToken: (token: string) => Boolean
  isAdmin: (token: string) => Boolean
}
