export interface EmailServiceRequest {
  to: string
  subject: string
  text: string
}

export interface EmailService {
  sendEmail(body: EmailServiceRequest): Promise<Boolean>
}
