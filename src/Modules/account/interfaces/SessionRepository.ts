import { Session } from './Session'

export interface SessionRepository {
  createSession(session: Session): Promise<Session | string>
  findSession(session: Session): Promise<Session | null>
  deleteSession: (session: Session) => Promise<Boolean>
}
