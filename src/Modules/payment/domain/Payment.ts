import { PaymentStatus } from './PaymentInfo'

export interface Payment {
  paymentId: string
  orderId: number
  status: PaymentStatus
  issueDate: Date
  currencyId: number
  companyId: number
  paymentMethodId: number
}
