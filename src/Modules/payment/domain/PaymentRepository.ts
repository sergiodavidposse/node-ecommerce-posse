import { UpdatePaymentStatusRequest } from '../application/UpdatePaymentStatusRequest'
import { Payment } from './Payment'
import { PaymentStatus } from './PaymentInfo'
import { PaymentWithRelations } from './PaymentWithRelations'

export interface PaymentRepository {
  createPayment(paymentId: string, orderId: number): Promise<Payment | null>
  findPaymentByOrderId(orderId: number): Promise<PaymentWithRelations | null>
  updatePaymentStatus(paymentId: string, status: PaymentStatus): Promise<Boolean>
}
