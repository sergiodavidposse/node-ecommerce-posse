export type PaymentStatus =
  | 'created'
  | 'approved'
  | 'pending'
  | 'authorized'
  | 'in_process'
  | 'in_mediation'
  | 'cancelled'
  | 'rejected'
  | 'refunded'
  | 'charged_back'

export interface PaymentInfo {
  status: PaymentStatus
  date_approved: string
  date_created: string
  description: string
  money_release_date: string
  money_release_schema: any
  money_release_status: string
  operation_type: string
  order: {
    id: string
    type: string
  }
  payer: {
    identification: {
      number: string
      type: string
    }
    entity_type: any
    phone: {
      number: string | null
      extension: string | null
      area_code: string | null
    }
    last_name: string | null
    id: string
    type: string | null
    first_name: string | null
    email: string
  }
  payment_method: {
    id: string
    issuer_id: string
    type: string
  }
  payment_method_id: string
  payment_type_id: string
  refunds: any[]
  date_last_updated: string
  transaction_details: {
    acquirer_reference: any
    external_resource_url: any
    financial_institution: any
    installment_amount: number
    net_received_amount: number
    overpaid_amount: number
    payable_deferral_period: any
    payment_method_reference_id: any
    total_paid_amount: number
  }
}
