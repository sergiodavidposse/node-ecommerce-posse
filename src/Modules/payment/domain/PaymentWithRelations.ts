import { Order } from '../../../Modules/order/domain/Order'
import { Currency } from '../../../Modules/currency/domain/Currency'
import { Company } from '../../../Modules/company/domain/Company'
import { PaymentMethod } from '../../../Modules/paymentMethod/domain/PaymentMethod'
import { PaymentStatus } from './PaymentInfo'

export interface PaymentWithRelations {
  order: Order
  status: PaymentStatus
  issueDate: Date
  currency: Currency
  company: Company
  paymentMethod: PaymentMethod
  paymentId: string
}
