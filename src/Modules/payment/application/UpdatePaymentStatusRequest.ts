import { PaymentStatus } from "../domain/PaymentInfo"

export interface UpdatePaymentStatusRequest {
  paymentId: number
  paymentStatus: PaymentStatus
}
