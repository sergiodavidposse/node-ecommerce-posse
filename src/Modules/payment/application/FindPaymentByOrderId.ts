import { PaymentRepository } from '../domain'

export class FindPaymentByOrderId {
  constructor(private readonly paymentRepository: PaymentRepository) {}
  public async execute(orderId: number) {
    const result = await this.paymentRepository.findPaymentByOrderId(orderId)
    return result
  }
}
