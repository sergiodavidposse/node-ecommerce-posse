import { PaymentRepository } from '../domain'
import { PaymentStatus } from '../domain/PaymentInfo'

export class UpdatePaymentStatus {
  constructor(private readonly paymentRepository: PaymentRepository) {}
  public async execute(paymentId: string, status: PaymentStatus) {
    const result = await this.paymentRepository.updatePaymentStatus(paymentId, status)
    return result
  }
}
