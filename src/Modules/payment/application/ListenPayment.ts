import { ProcessOrderService } from '../../../Modules/order/domain/ProcessOrderService'
import { PaymentRepository } from '../domain'

export class ListenPayment {
  constructor(
    private mercadoPagoService: ProcessOrderService,
    private paymentRepository: PaymentRepository
  ) {}
  public async execute(paymentId: string) {
    const paymentInfo = await this.mercadoPagoService.getPaymentInfoAPI(paymentId)
    if (!paymentInfo?.status)
      throw new Error('error getting payment info, payment status not found')

    const result = await this.paymentRepository.updatePaymentStatus(paymentId, paymentInfo.status)
    console.log(new Date(), paymentInfo)
    return result
  }
}
