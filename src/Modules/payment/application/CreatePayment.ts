import { PaymentRepository } from '../domain/PaymentRepository'

export class CreatePayment {
  constructor(private readonly paymentRepository: PaymentRepository) {}
  public async execute(paymentId: string, orderId: number) {
    const result = await this.paymentRepository.createPayment(paymentId, orderId)
    return result
  }
}
