import container, { containerNames } from './Infra/container-dependency-injection/index'
import { Server } from './Infra/Server'

const app: Server = container.get(containerNames.Server.Express)

app.start()
