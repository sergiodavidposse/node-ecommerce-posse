module.exports = {
  env: {
    es2021: true,
    node: true,
    'jest/globals': true
  },
  extends: ['standard'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: ['prettier', 'jest'],
  rules: {
    'no-useless-constructor': 0,
    camelcase: 'off'
  }
}
