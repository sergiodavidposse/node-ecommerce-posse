export default {
  preset: 'ts-jest', // Indica que vamos a usar ts-jest para procesar TypeScript
  testEnvironment: 'node', // Define el entorno de ejecución
  verbose: true, // Muestra más detalles al correr las pruebas
  moduleDirectories: ['node_modules', 'src'], // Si tienes una carpeta src para evitar rutas largas
  testMatch: ['**/src/**/*.test.ts'], // Ajusta la extensión según el lenguaje que uses
}